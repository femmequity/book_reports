![Remote](https://tonomoshiadotcom.files.wordpress.com/2017/05/remote.jpg)

## 10 Things I Learned from Remote

Okay, so I finished this book more than a month ago. It has been ruminating in my mind ever since and I have been reading other ancillary resources and kind of gathering them up like a camping kit to go in the wilderness.

I have a bunch of them I would like to share with you.

But first . . . what about the book?

Well, I did enjoy it. I thought it did a good job of bringing up and defeating the ruses companies use to avoid going remote.

If only such arguments would work at my company. Well, that's not fair. They have actually forced some departments to go remote while denying the privilege to others. And make no mistake they see it as a privilege.

As the book says though, there are many benefits for the company and the worker. SO who is getting the privilege, or can we share?

Some of the benefits:

1.

2.

3.

#### Some things I learned

1. For a team to be in sync, a four-hour overlap in timezones is helpful.

2. Having as much as you can out in the open is helpful. I listened to the [Yonder podcast with the CEO of Baremetrics, Josh Pigford](https://www.yonder.io/post/2017/6/22/episode-17-baremetrics-josh-pigford) and he mentioned they do not use email for any internal communications and it makes sense. When I send email I do not send it to everyone, just to the person or few persons that I am communicating with. If that is company information that might be helpful to others, it is locked in individual email boxes. It is much better to have it where it can be accessed by whomever may need it without barriers to access. At my company they have tried to push the use of Sharepoint, but unfortunately it is not really catching on. At Baremetrics they use Slack for all internal communications so that it can be searched and read by everyone when they need it. They use Dropbox Paper for shared documents. In the book, of course, they push their product, Basecamp, which is pretty good too. I think Quip is also good for this. Or HipChat with Confluence if you are using Atlassian products.

### Resources for My "Remote Resources" Page

#### Great podcasts about remote work

##### All [Yonder](https://www.yonder.io/) podcasts, including:

1. [Yonder Episode 15--Todd Ross Nienkerk](https://www.yonder.io/post/2017/5/25/episode-15-todd-ross-nienkerk) about transitioning from brick-and-mortar to distributed and pitfalls of a hybrid company.

2. [Yonder Episode 9--Lara Owen of Github](https://www.yonder.io/post/2017/3/2/episode-9-github-lara-owen) of managing communication, culture, and productivity for GitHub's team of over 600 employees.

3. [Yonder Podcast with Flexjobs' Brie Reynolds](https://www.yonder.io/post/2016/10/13/episode-6-flexjobs-brie-reynolds)

4. [Yonder Podcast with Buffer's Rodolphe Dutel](https://www.yonder.io/post/2016/8/18/yonder-podcast-ep2-buffers-rodolphe-dutel)

5. [Yonder Podcast with Baremetrics' Josh Pigford](https://www.yonder.io/post/2017/6/22/episode-17-baremetrics-josh-pigford) about running a distributed company without email and very few meetings.


##### Other podcasts on remote work:

1.

2.

3.




All-day seminar on remote worK: [HumanMade seminar](http://www.outofoffice.hm/) regarding remote working (Out of the Office)


#### Remote job sites



[50 Tools for Remote Working](https://refuga.com/blog/50-tools-remote-working/)
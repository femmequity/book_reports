<style>
  body          { margin:40px; background-color:lightskyblue;}
  a:link        { color: green; }
  a:visited     { color: black; }
  a:hover       { color: blue; }
  a:active      { color: fuchsia; }
  code          { background-color: #f8f8f8; padding:5px;}
  li            { margin:5px; color: #000; font-family: Arial;}
  p             {margin:30px 0; color: #000; font-family: Arial;}
  h1, h2, h3, h4, h5, h6            { color: indigo; font-family: Arial;}
img[alt=Photo-by-insung-yoon-on-Unsplash] { width: 60%; border: none; background: none; display: block; margin-left: auto; margin-right: auto;}
img[alt=tonomoshia_logo] { width: 60%; border: none; background: none; display: block; margin-left: auto; margin-right: auto;}
</style>

# README #

### What is this repository for? ###

I will be reading books about agile/scrum/kanban/lean, clean coding, and how to be a better developer. I will be writing "book reports" about what I learn from these books. I will post these books reports to [my WordPress website][1] as **"10 Things I Learned from X"**.

This is my first repository in **Bitbucket**. I am also using the Book Reports to learn how to use Bitbucket as so far I have only used Github.

SInce I am drafting my book reports in markdown, it is also an opportunity to get more familiar with markdown.

### Book List

#### Already Read

1. [Remote: Office Not Required](Remote.md) - [ ] KINDLE - [X] HARDCOPY
2. [Turn This Ship Around and the workbook, Turn Your Ship Around](turn_this_ship_around.md). Reading this with the Agile Florida Book Club. - [ ] KINDLE - [X] HARDCOPY
3. [Deep Work](deepwork.md) - [X] KINDLE - [ ] HARDCOPY
4. [The Great Scrummaster: #ScrumMasterWay](great_scrum_master.md) - [ ] KINDLE - [x] HARDCOPY
5. [Do the Work: Overcome Resistance and Get Out of Your Own Way](do_the_work.md) - [x] KINDLE - [ ] HARDCOPY
6. [War of Art by Steven Pressfield](war_of_art.md) - [x] KINDLE - [ ] HARDCOPY

####  In Progress
1. [The Developer's Code: What Real Programmers Do](dev_code.md) - [ ] KINDLE - [X] HARDCOPY (from library)
2. [The Lean Startup: How Today's Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses](lean_startup.md) - [X] KINDLE - [X] HARDCOPY
3. [Create Your Own Successful Agile Project](create_your_successful_agile_project.md) - [ ] KINDLE - [X] HARDCOPY
4. [Dealing with Problem Clients](dealing_with_problem_clients.md) - [X] KINDLE - [X] HARDCOPY
5. [Company of One](company_of_one.md) - [X] KINDLE - [ ] HARDCOPY
6. [Book Yourself Solid](book_yourself_solid.md) - [ ] KINDLE - [X] HARDCOPY
7. [Agile Retrospectives: Making Good Teams Great by Esther Derby and Diana Larsen](agile_retrospectives.md) - [ ] KINDLE - [x] HARDCOPY
8. [Managing for Happiness: Games, Tools, and Practices to Motivate Any Team by Jurgen Appelo](managing_for_happiness.md) - [ ] KINDLE - [x] HARDCOPY

#### To Read

1. The ONE Thing: The Surprisingly Simple Truth Behind Extraordinary Results - [x] KINDLE - [ ] HARDCOPY
2. Rework - [x] KINDLE - [x] HARDCOPY
3. Tools of Titans: The Tactics, Routines, and Habits of Billionaires, Icons, and World-Class Performers - [ ] KINDLE - [x] HARDCOPY
4. Crucial Conversations Tools for Talking When Stakes Are High. Apparently this has been read by most people involved in Agile. Book that follows is Crucual Accountability which I also have as a download and a hardcopy. - [x] KINDLE - [ ] HARDCOPY
5. Mastery by Robert Greene - [x] KINDLE - [ ] HARDCOPY
6. The 10X Rule: The Only Difference Between Success and Failure - [ ] KINDLE - [ ] HARDCOPY - [x] AUDIO
7. Influence: The Psychology of Persuasion by Robert Cialdini- [x] KINDLE - [ ] HARDCOPY
10. Programming Beyond Practices by Gregory Brown - [ ] KINDLE - [ ] HARDCOPY
11. The Clean Coder: A Code of Conduct for Professional Programmers - [ ] KINDLE - [x] HARDCOPY
12.  From Chaos to Successful Distributed Agile Teams: Collaborate to Deliver by Johanna Rothman and Mark Kilby - [ ] KINDLE - [x] HARDCOPY

### Idea(s) for Reward(s) Upon Successful Completion of Book Report(s)
###### these are moot as I already have both of these items now
~~1. [AncestryDNA](http://amzn.to/2tBbZsa) or [23andMe DNA](http://amzn.to/2tBArK6) test kit~~

~~2. [TP-Link Smart LED Light Bulb](http://amzn.to/2sZ0DfB)~~
1. Ledger Nano X

* * *


###### Affiliate Disclaimer

I’ve always believed in transparency on the web and so I am disclosing that I’ve included certain books and products and links to those products on this site that I will earn an affiliate commission for any purchases you make.

You should assume that any links leading you to products or services are affiliate links that I will receive compensation from just to be safe. I only promote those products or services that I have investigated and truly feel deliver value to you.

Please note that I have not been given any free products, services or anything else by these companies in exchange for mentioning them on this page. I have paid for all my own books/products.


[1]: https://tonomoshia.com "Tonomoshia.com"
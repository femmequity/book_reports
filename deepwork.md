Deep Work by Cal Newport

"Very few people work even 8 hours a day. You're lucky if you get a few good hours in between all the meetings, interruptions, web surfing, office politics, and personal business that permeate the typical workday. Fewer official working hours help squeeze the fat out of the typical workweek. Once everyone has less time to get their stuff done, they respect that time even more. People become stingy with their time and that's a good thing."
page 216

You ever have a vacation planned and realize you have so much work to be done before you set sail or take flight? Do you recall the flurry of activity, the vast amount of work you are able to get done with a looming, (but pleasant) deadline before you?

When Basecamp squeezed the 5 day, 40 hour work week to a 4 day, 32 hour work week, they achieved the same effect. People have the tendency to focus more because they have less time to do the same amount of things. Shallow work falls to the wayside while deep work comes to the forefront.